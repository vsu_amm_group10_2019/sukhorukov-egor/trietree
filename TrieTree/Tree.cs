﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    class Tree
    {
        public Node Root { get; set; } = new Node();
        public void Add(string value) 

        {
            Root.Add(value.ToLower());
        }
        public void PrintToTreeView (TreeView treeView)
        {
            if (Root != null)
            {
                treeView.Nodes.Add("");
                Root.PrintToTreeNode(treeView.Nodes[0]);
            }
        }
        public bool Delete(string word)
        {
            if (Root != null)
            {
                if (Root.Delete(word.ToLower()))
                {
                    return true;
                }else
                    return false;
            }else
                return false;
        }
        public int CountWord (char c)
        {
            int count = Root.CountWord(c);
            return count;
        }
    }
}
