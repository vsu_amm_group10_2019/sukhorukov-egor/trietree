﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    public partial class ReadForm : Form
    {
        FormState  FormState;
        public string txt;
        public bool ok = false;
        public char c;
        public ReadForm(FormState formState)
        {
            InitializeComponent();
            FormState = formState;
            switch (formState)
            {
                case FormState.ADD:
                    {
                        text.Text = "Введите добавляемое слово";
                        break;
                    }
                case FormState.DELETE:
                    {
                        text.Text = "Введите удаляемое слово";
                        break;
                    }
                case FormState.SEARCH:
                    {
                        text.Text = "Введите букву";
                        break;
                    }
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (FormState == FormState.ADD)
            {
                txt = textBox.Text;
                ok = true;
                Close();
            }
            else
                if (FormState == FormState.DELETE)
                {
                    txt = textBox.Text;
                    ok = true;
                    Close();
                }
                else
                    if (FormState == FormState.SEARCH)
                    {
                        try
                        {
                            c = Convert.ToChar(textBox.Text);
                            ok = true;
                            Close();
                        }
                        catch
                        {
                            MessageBox.Show(
                            "Поле заполнено не верно!",
                            "Ошибка ввода!",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        }
                    }
        }   
    }
}
