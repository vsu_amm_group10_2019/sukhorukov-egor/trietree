﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    public partial class MainForm : Form
    {
        Tree Tree = new Tree();
        public MainForm()
        {
            InitializeComponent();
        }

        private void Redraw()
        {
            treeView1.Nodes.Clear();
            Tree.PrintToTreeView(treeView1);
            treeView1.ExpandAll();
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            string fileName = openFileDialog.FileName;
            string[] lines = File.ReadAllLines(fileName);
            foreach (string str in lines)
            {
                string[] words = str.Split(' ');
                foreach (string word in words)
                {
                    string tmp = word.Trim();
                    if (!string.IsNullOrEmpty(tmp))
                    {
                        Tree.Add(word);
                    }
                }
            }
            Redraw();
        }

        private void count_Click(object sender, EventArgs e)
        {
            ReadForm readForm = new ReadForm(FormState.SEARCH);
            readForm.ShowDialog();
            if (readForm.ok)
            {
                MessageBox.Show("Количество слов, содержащих букву '" + readForm.c + "' = " + Convert.ToString(Tree.CountWord(readForm.c)));
            }
        }

        private void add_Click(object sender, EventArgs e)
        {
            ReadForm readForm = new ReadForm(FormState.ADD);
            readForm.ShowDialog();
            if (readForm.ok)
            {
                Tree.Add(readForm.txt);
                Redraw();
            }
        }

        private void delete_Click(object sender, EventArgs e)
        {
            ReadForm readForm = new ReadForm(FormState.DELETE);
            readForm.ShowDialog();
            if (readForm.ok)
            {
                if (Tree.Delete(readForm.txt))
                {
                    Redraw();
                }
                else
                {
                    MessageBox.Show("Данного слова нет");
                }
            }
        }
    }
}
